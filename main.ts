import { GetProductsForIngredient, GetRecipes } from "./supporting-files/data-access";
import { NutrientFact, Product, Recipe, RecipeLineItem, SupplierProduct } from "./supporting-files/models";
import { GetCostPerBaseUnit, GetNutrientFactInBaseUnits } from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */


//Created an interface to secure that functions needing this one is receiving the right parameter
interface CostAndNutrientFactsInBaseUnit {
    costInBaseUnit: number;
    nutrientFactsInBaseUnit: NutrientFact[];
}

//This function is created to get the cheapest item from the Products. Reduce function is use simply to compare the prices and return the cheapest one
const GetCheapestItem = (items: CostAndNutrientFactsInBaseUnit[]) => items.reduce((cheapest, item) => item.costInBaseUnit < cheapest.costInBaseUnit ? item : cheapest);

//This function is created to get the Cost of each ingredient on the item line, also returns the nutrientfacts about the ingredient
const GetCostAndNutrientFactsInBaseUnit = (products: Product[]) =>
    products.map((product: Product) => {

        //This one will return the cost of a product from each supplier. The GetCostPerBaseUnit is a helper functions which minifies the difficulty of the challenge
        const suppliersPrices = product.supplierProducts.map((supplierProduct: SupplierProduct) => GetCostPerBaseUnit(supplierProduct))

        //Returns the nutrient facts of each product
        const nutrientFactsInBaseUnit = product.nutrientFacts.map((nutrientFact: NutrientFact) => GetNutrientFactInBaseUnits(nutrientFact))
        return {
            // min function will return the cheapest cost
            costInBaseUnit: Math.min(...suppliersPrices),
            nutrientFactsInBaseUnit: nutrientFactsInBaseUnit
        }
    });

//This function is craeted to simply sort the nutrientFacts of a recipe
const SortNutrientFacts = (nutrients: any) => Object.keys(nutrients).sort().reduce((obj, key) => { obj[key] = nutrients[key]; return obj; }, {})

//This is the main function that gets the summarry of a recipe at its cheapest cost
const GetSummaryForCheapestCost = (recipe: Recipe) => {
    const result = {
        cheapestCost: 0,
        nutrientsAtCheapestCost: {}
    }
    const { lineItems } = recipe
    lineItems.forEach((item: RecipeLineItem) => {

        //GetProductsForIngredient -> helper function
        const products = GetProductsForIngredient(item.ingredient)

        //Next two are using the functions created above
        const costAndNutrientFactsInBaseUnit = GetCostAndNutrientFactsInBaseUnit(products)
        const cheapestItem = GetCheapestItem(costAndNutrientFactsInBaseUnit)


        const unitPerItem = item.unitOfMeasure.uomAmount
        result.cheapestCost += (unitPerItem * cheapestItem.costInBaseUnit)

        //This iteration will check all the nutrientFacts from each product, if same nutrient got from different product it will add the quantitiy amount
        cheapestItem.nutrientFactsInBaseUnit.forEach((nutrientFact: NutrientFact) => {
            if (result.nutrientsAtCheapestCost.hasOwnProperty(nutrientFact.nutrientName)) {
                result.nutrientsAtCheapestCost[nutrientFact.nutrientName].quantityAmount.uomAmount += nutrientFact.quantityAmount.uomAmount;
                return
            }
            result.nutrientsAtCheapestCost[nutrientFact.nutrientName] = nutrientFact
        })
    })

    //Sort the Nutrients Facts using the Function created above
    result.nutrientsAtCheapestCost = SortNutrientFacts(result.nutrientsAtCheapestCost)
    return result
}


//There is only one recipe for now, but this function will loop through the recipe if others are added.
recipeData.forEach((recipe: Recipe) => {
    recipeSummary[recipe.recipeName] =
        GetSummaryForCheapestCost(recipe);
})

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
